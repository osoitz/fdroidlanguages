#Copyright (C) 2018 Osoitz Elkorobarrutia

#This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.


apkFileURL=$1 #URL of the file to download
apkFile=${apkFileURL##*/} #name of the downloaded file
outputTempFolder="out" #name of the folder that contains the extracted apk
searchedString="*strings.xml" #the substring we search to find language files
defaultCode="00" #code assigned to the default language, usually it's actually English
outputFile="${apkFile%%.apk}-languages.csv"

#remove destination folder and apk file if they already exist
rm $apkFile
rm -rf $outputTempFolder
wget $apkFileURL
apktool d $apkFile -o $outputTempFolder


languageFiles=($(find "$outputTempFolder" -name "$searchedString"))

for languageFilePath in "${languageFiles[@]}"
  do
    #If the path contains the - character we extract the language code, else we asumme its the default 'values' path
    if [[ $languageFilePath == *values-* ]];
      then
        languageCode="${languageFilePath##*values-}"
        languageCode="${languageCode%%/*}"
      else
        languageCode="$defaultCode"
    fi

    lineCount=($(wc -l "$languageFilePath"))
    echo "$languageCode,$lineCount" >> $outputFile

  done

